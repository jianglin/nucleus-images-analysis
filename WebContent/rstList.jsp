<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" href="css/rstListCSS.css" type="text/css"
	media="all" />

<script language="javascript" src="rstListJS.js"></script>

<title>Result List</title>
</head>
<body>
<h3>Result for query is:</h3><br>
<center>
	<table class="altrowstable" id="rstTable">
	</table>
	</center>
</body>

</html>