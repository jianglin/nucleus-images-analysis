/**
 * rstList.jsp js
 */
function altRows(id) {
	if (document.getElementsByTagName) {

		var table = document.getElementById(id);
		var rows = table.getElementsByTagName("tr");

		for (i = 0; i < rows.length; i++) {
			if (i % 2 == 0) {
				rows[i].className = "evenrowcolor";
			} else {
				rows[i].className = "oddrowcolor";
			}
		}
	}
}

window.onload = function() {
	var content = window.opener.rstContent;
	window.opener.rstContent = null;
	createTable('rstTable',content);
	altRows('rstTable');
}

function createTable(id, content) {
	var records=eval(content);  
	var data = new Array();
	data.push('<table border=1><tbody>');
	data.push('<tr><th>Number</th><th>Details</th><th>View Image</th></tr>');
	for (var i = 0; i < records.length; i++) {
		data.push('<tr>');
		data.push('<td>' + (i+1)+ '</td>');
		data.push('<td>' + records[i].content+ '</td>');
		data.push( '<td style="cursor:pointer;" onclick="requestImg('+'\''+records[i].rowKey+'\''+')">' +'Click to view '+records[i].rowKey+'</td>');
		data.push('</tr>');
	}
	data.push('</tbody><table>');

	document.getElementById(id).innerHTML = data.join('');
}
function requestImg(id){
	alert(id);
}