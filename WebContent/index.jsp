<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.List,tpt.info.PageSelectionInfo"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Expires" content="0" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" href="css/all.css" type="text/css" media="all" />
<link rel="stylesheet" href="assets/css/amazeui.min.css" />

<script language="javascript" src="indexJS.js"></script>
<script src="assets/js/zepto.min.js"></script>
<script src="assets/js/amazeui.min.js"></script>

<title>Tpt</title>

</head>
<body class="dampage download_data findarchivesform">

	<!-- END NCI Banner -->
	<!-- Masthead (Logo, utility links, search) -->
	<div id="masthead">
		<!--[if lt IE 7]>
<style type="text/css"><!--
#logo {
{ behavior: url(htc/iepngfix.htc);
}
-->
		</style>
		<![endif]-->

		<!--div id="search">
		<form action="http://cancergenome.nih.gov/search" method="post" target="_blank">
			<input alt="Search TCGA site" class="main-search" id="swKeywordQuery" maxlength="255" name="swKeywordQuery" value=" Search"  class="main-search" onfocus="value	=''" />
			<input type="image" id="schImg" src="/tcga/images/general/search-btn.gif" alt="Search" />
		</form>
	</div-->
	</div>
	<!-- END Masthead (Logo, utility links, search) -->
	<!-- Main Navigation -->
	<!-- From release 1.30, There is a copy version of this in ccg-data-web: tcganav.jspf. the only change there is the host name should be tcga instead of ccg.
     So no matter what change in this mainnav should also reflect in tcganav in ccg-data-web.
-->
	<!--[if lt IE 7]>
<style type="text/css"><!--
#mainnav ul {
	margin-bottom: -7px;
}
-->
	</style>
	<![endif]-->
	<!-- Container for Content and Sidebar -->
	<div id="container">
		<div id="content" class="dam">
			<div class="trail">
				<p>Project 4</p>
				> <span class="trailDest">Group 8</span>
			</div>
			<a name="skip" id="skip"></a>
			<h1>Please select and input search condition:</h1>

			<form>
				<div class="archiveTable designBody">

					<label for="cancerType"><h3 class="archiveTableLeft">Project</h3></label>

					<div class="archiveTableRight">
						<select name="Project" id="project" multiple="multiple" size="5">
							<option value="-1" selected>All</option>
							<%
								List<String> projects = PageSelectionInfo
										.getInfoList(PageSelectionInfo.Project);
								for (int i = 0; i < projects.size(); i++) {
									out.print("<option value=\"" + (i + 1) + "\">"
											+ projects.get(i) + "</option>");
								}
							%>
						</select>
					</div>
					<div class="clear"></div>

					<label for="center"><h3 class="archiveTableLeft">TSS</h3></label>

					<div class="archiveTableRight">
						<select name="tss" id="tss" multiple="multiple" size="5">
							<option value="-1" selected>All</option>
							<%
								List<String> tss = PageSelectionInfo
										.getInfoList(PageSelectionInfo.TSS);
								for (int i = 0; i < tss.size(); i++) {
									out.print("<option value=\"" + (i + 1) + "\">" + tss.get(i)
											+ "</option>");
								}
							%>

						</select>
					</div>
					<div class="clear"></div>


					<label for="platform"><h3 class="archiveTableLeft">Participant</h3></label>

					<div class="archiveTableRight">
						<select name="participant" id="participant" multiple="multiple"
							size="5">
							<option value="-1" selected>All</option>
							<%
								List<String> participants = PageSelectionInfo
										.getInfoList(PageSelectionInfo.Paticipant);
								for (int i = 0; i < participants.size(); i++) {
									out.print("<option value=\"" + (i + 1) + "\">"
											+ participants.get(i) + "</option>");
								}
							%>
						</select>
					</div>
					<div class="clear"></div>
					<label for="dataType"><h3 class="archiveTableLeft">Sample</h3></label>

					<div class="archiveTableRight">
						<select name="sample" id="sample" multiple="multiple" size="5">
							<option value="-1" selected>All</option>
							<%
								List<String> samples = PageSelectionInfo
										.getInfoList(PageSelectionInfo.Sample);
								for (int i = 0; i < samples.size(); i++) {
									out.print("<option value=\"" + (i + 1) + "\">" + samples.get(i)
											+ "</option>");
								}
							%>
						</select>
					</div>
					<div class="clear"></div>


					<label for="archiveType"><h3 class="archiveTableLeft">Vial</h3></label>

					<div class="archiveTableRight">
						<select name="vial" id="vial" multiple="multiple" size="5">
							<option value="-1" selected>All</option>
							<%
								List<String> vial = PageSelectionInfo
										.getInfoList(PageSelectionInfo.Vial);
								for (int i = 0; i < vial.size(); i++) {
									out.print("<option value=\"" + (i + 1) + "\">" + vial.get(i)
											+ "</option>");
								}
							%>
						</select>
					</div>
					<div class="clear"></div>
					
					<label for="archiveType"><h3 class="archiveTableLeft">Portion</h3></label>
					<div class="archiveTableRight">
						<select name="portion" id="portion" multiple="multiple" size="5">
							<option value="-1" selected>All</option>
							<%
								List<String> portion = PageSelectionInfo
										.getInfoList(PageSelectionInfo.Portion);
								for (int i = 0; i < portion.size(); i++) {
									out.print("<option value=\"" + (i + 1) + "\">" + portion.get(i)
											+ "</option>");
								}
							%>
						</select>
					</div>
					<div class="clear"></div>
					<div class="clear"></div>


					<div class="archiveTableLeft">
						<label for="fileName"><h3>Feature Query:</h3></label>
					</div>
					<div class="archiveTableRight">
						<textarea rows="9" cols="158" id="query" name="query"><% out.print(PageSelectionInfo.defaultQuery); %></textarea>
					</div>
					<div class="clear"></div>

					<div class="clear"></div>

					<div class="footer">
						<input type="reset" value="Reset" class="button left" /><input
							type="button" value="Find >>" class="button right"
							onclick="submitForm('project','tss','participant','sample','vial','portion')" />

					</div>
					<div class="clear"></div>
				</div>
			</form>
		</div>
	</div>

	<!-- This is loading -->
	<div class="am-modal am-modal-loading am-modal-no-btn" tabindex="-1"
		id="my-modal-loading">
		<div class="am-modal-dialog">
			<div class="am-modal-hd">Search...</div>
			<div class="am-modal-bd">
				<span class="am-icon-spinner am-icon-spin"></span>
			</div>
		</div>
	</div>

</body>
</html>
