# README #

Store and organize nucleus slide images data in Hadoop HBase. Develop analysis functions to process nucleus data and images, and visualize results in web application for pathologists.

### Languages ###

* Java
* JavaServer Pages
* JavaScript