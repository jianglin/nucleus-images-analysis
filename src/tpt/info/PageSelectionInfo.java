package tpt.info;

import java.util.LinkedList;
import java.util.List;

public class PageSelectionInfo {

	public static String[] Project = { "TCGA" };
	public static String[] TSS = { "06", "08", "12", "14", "19", "27", "28",
			"41" };
	public static String[] Paticipant = { "0127", "0150", "0152 ", "0166 ",
			"0168",
			"0171 ", "0173 ", "0174 ", "0176 ", "0178", "  0179", "  0184",
			"  0187", "  0188", "  0189", "  0190", "  0197", "  0201",
			"  0206", " 0208 ", "0209 ", "0210 ", "0211 ", "0213 ", "0214 ",
			"0216 ", "0219 ", "0221 ", "0237 ", "0238 ", "0241 ", "0352 ",
			"0644 ", "0645 ", "0646 ", "0648 ", "0656 ", "0657 ", "0686 ",
			"0703 ", "0750 ", "0773 ", "0957 ", "1037 ", "1385 ", "1386 ",
			"1387 ", "1388 ", "1389 ", "1455 ", "1458 ", "1745 ", "1746 ",
			"1749 ", "1751 ", "1752 ", "1755 ", "1757 ", "1760 ", "1786 ",
			"1788 ", "1789 ", "1790 ", "1791 ", "1794 ", "1795 ", "1801 ",
			"1802 ", "1805 ", "1821 ", "1823 ", "1825 ", "1827 ", "1829 ",
			"1830 ", "1831 ", "1832 ", "1833 ", "1834 ", "1835 ", "1836 ",
			"1837 ", "1838 ", "2572 ", "2631 ", "3393 ", "3915 ", "4065 ",
			"4068 ", "4097 ", "5414 ", "5415 ", "5416 ", "5417", "5418" };
	public static String[] Sample = { "01" };
	public static String[] Vial = { "Z" };
	public static String[] Portion = { "00" };
	
	public static String defaultQuery = "Perimeter=' '; Eccentricity=' '; Circularity=' ';"
			+ "MajorAxisLength=' '; MinorAxisLength=' '; Extent=' '; Solidity=' '; FSD1=' ';"
			+ "FSD2=' '; FSD3=' '; FSD4=' '; FSD5=' '; FSD6=' '; HematoxlyinMeanIntensity=' ';"
			+ "HematoxlyinMeanMedianDifferenceIntensity=' '; HematoxlyinMaxIntensity=' ';"
			+ "HematoxlyinMinIntensity=' '; HematoxlyinStdIntensity=' '; HematoxlyinEntropy=' ';"
			+ "HematoxlyinEnergy=' '; HematoxlyinSkewness=' '; HematoxlyinKurtosis=' ';"
			+ "HematoxlyinMeanGradMag=' '; HematoxlyinStdGradMag=' '; HematoxlyinEntropyGradMag=' ';"
			+ "HematoxlyinEnergyGradMag=' '; HematoxlyinSkewnessGradMag=' '; HematoxlyinKurtosisGradMag=' ';"
			+ "HematoxlyinSumCanny=' '; HematoxlyinMeanCanny=' '; CytoplasmMeanIntensity=' ';"
			+ "CytoplasmMeanMedianDifferenceIntensity=' '; CytoplasmMaxIntensity=' ';"
			+ "CytoplasmMinIntensity=' '; CytoplasmStdIntensity=' '; CytoplasmEntropy=' ';"
			+ "CytoplasmEnergy=' '; CytoplasmSkewness=' '; CytoplasmKurtosis=' '; CytoplasmMeanGradMag=' ';"
			+ "CytoplasmStdGradMag=' '; CytoplasmEntropyGradMag=' '; CytoplasmEnergyGradMag=' ';"
			+ "CytoplasmSkewnessGradMag=' '; CytoplasmKurtosisGradMag=' '; CytoplasmSumCanny=' ';"
			+ "CytoplasmMeanCanny=' ';";

	public static List<String> getInfoList(String[] info) {
		List<String> result = new LinkedList<String>();
		for (int i = 0; i < info.length; i++) {
			result.add(info[i]);
		}
		return result;
	}

}
