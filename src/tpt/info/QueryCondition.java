package tpt.info;

public class QueryCondition {
	int errorCode;
	String variable;
	String operator;
	String value;

	public QueryCondition(String v, String o, String val) {
		variable = v;
		operator = o;
		value = val;
	}

	public String getVariable() {
		return variable;
	}

	public String getOperator() {
		return operator;
	}

	public String getValue() {
		return value;
	}

	public void setErrorMessage(String err) {
		variable = err;
	}

}
