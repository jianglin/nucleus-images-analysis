package tpt.hbase;

import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

class ImageClip {
	public static BufferedImage clip( BufferedImage image, float... clipVerts ) /* {x1,y1,x2,y2...} value between 0-1*/
	  {
	    assert clipVerts.length >= 6;
	    assert clipVerts.length % 2 == 0;

	    int[] xp = new int[ clipVerts.length / 2 ];
	    int[] yp = new int[ xp.length ];

	    int minX = image.getWidth(), minY = image.getHeight(), maxX = 0, maxY = 0;

	    for( int j = 0; j < xp.length; j++ )
	    {
	      xp[ j ] = Math.round( clipVerts[ 2 * j ] * image.getWidth() );
	      yp[ j ] = Math.round( clipVerts[ 2 * j + 1 ] * image.getHeight() );

	      minX = Math.min( minX, xp[ j ] );
	      minY = Math.min( minY, yp[ j ] );
	      maxX = Math.max( maxX, xp[ j ] );
	      maxY = Math.max( maxY, yp[ j ] );
	    }

	    for( int i = 0; i < xp.length; i++ )
	    {
	      xp[ i ] -= minX;
	      yp[ i ] -= minY;
	    }

	    Polygon clip = new Polygon( xp, yp, xp.length );
	    BufferedImage out = new BufferedImage( maxX - minX, maxY - minY, image.getType() );
	    Graphics g = out.getGraphics();
	    g.setClip( clip );

	    g.drawImage( image, -minX, -minY, null );
	    g.dispose();

	    return out;
	  }
	public static float[] getCoords(String coords)
	{
		System.out.println("Into getCoords");
		String temp=coords;
		String[] splitString = temp.split("[,;]");
		float[] coord=new float[splitString.length];
        for (int i=0; i<splitString.length; i++)
        {
        	coord[i]=Float.parseFloat(splitString[i]);
        }
        return coord;
	}
	public static BufferedImage processImage(String path,String coords){
		BufferedImage img = null;
		BufferedImage result = null;
		try {
			img = ImageIO.read(new File(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		float coordinate[]=getCoords(coords);
		
		for(int i=0;i<coordinate.length;i++)
        {
        	System.out.println(coordinate[i]);
        }
		//float coordinate[]={(float)0.3588,(float)0.0995,(float)0.1384,(float)0.8903,(float)0.0888,(float)0.1631,(float)0.2760,(float)0.1572};
		result = clip(img, coordinate);
		return result;
		
		
//		float a = 0.2497f;
//		System.out.println(a);
	}
}
