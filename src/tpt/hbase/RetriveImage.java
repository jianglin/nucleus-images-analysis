package tpt.hbase;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class RetriveImage {

	public static String getImagePath(String rowkey) {
		String path = "/data07/shared/matlabOutput/";
		File folder = new File(path);
		String org;
		File[] listOfOrgs = folder.listFiles();
		for (int k = 0; k < listOfOrgs.length; k++) {
			String org_path = path;
			org = listOfOrgs[k].getName();
			// System.out.println("The organization name:" + org);
			org_path = org_path + "/" + org;
			String slide;
			File[] listOfSlides = listOfOrgs[k].listFiles();

			for (int i = 0; i < listOfSlides.length; i++) {
				String slide_path = org_path;
				if (listOfSlides[i].isDirectory()) {
					slide = listOfSlides[i].getName();
					// System.out.println("	The slide name:" + slide);
					String slide_prefix = getSlideprefix(rowkey);
					if (slide.contains(slide_prefix)) {
						// System.out.println("		The slide name matched:" +
						// slide);
						slide_path = slide_path + "/" + slide;

						String segment_image;
						File[] listOfSegments = listOfSlides[i].listFiles();
						for (int j = 0; j < listOfSegments.length; j++) {
							// System.out.println("Inside Secondfor loop");
							String seg_path = slide_path;
							if (listOfSegments[j].isFile()) {
								segment_image = listOfSegments[j].getName();
								if (segment_image.endsWith(".jpg")) {
									String req_seg = getSegment(rowkey);
									if (segment_image.contains(req_seg)) {
										String result = seg_path + "/"
												+ segment_image;
										return result;
									}

								}
							}
						}
					}
				}
			}

		}

		return null;
	}

	public static void StoreResult(BufferedImage result) {
		cleanDestination("/data07/shared/result/");

		// File out = new
		// File("C:\\Users\\Ashwin\\workspace\\Data Analytics\\src\\imageClip\\src\\result.jpg");
		File out = new File("/data07/shared/result/result.jpg");
		try {
			ImageIO.write(result, "jpg", out);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void cleanDestination(String folder) {
		File result_folder = new File(folder);

		for (File file : result_folder.listFiles())
			if (!file.isDirectory())
				file.delete();

	}

	public static String getSlideprefix(String rowkey) {

		String temp = rowkey;
		String[] splitString = temp.split("\\:");
		String req_parts = splitString[0];
		return req_parts;
	}

	public static String getSegment(String rowkey) {

		String temp = rowkey;
		String[] splitString = temp.split("\\:");
		String req_parts = splitString[1];
		return req_parts;
	}

}
