package tpt.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tpt.info.QueryCondition;
import tpt.info.TransRecord;

/**
 * Servlet implementation class parseSelection
 */
@WebServlet("/parseSelection")
public class parseSelection extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public parseSelection() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String allSelection = request.getParameter("selection");
		if (allSelection != null && !allSelection.isEmpty()) {
			Map<String, String> parms = parseJson(allSelection);
			List<TransRecord> records = new ArrayList<TransRecord>(parms.size());
			String errorInfo = "";
			if (parms.containsKey("query")) {
				List<QueryCondition> queries = QueryConditionParse
						.parseQueryCondition(parms.get("query"));
				if (!queries.get(0).getVariable().isEmpty()) {
					// has error info
					errorInfo = queries.get(0).getVariable();
					response.getWriter().print(errorInfo);
					return;
				} else if (queries.size() == 1) {
					response.getWriter().print(
							"error: Please input query condition.");
					return;
				} else {
					queries.remove(0);
					for (QueryCondition qd : queries) {
						TransRecord transRecord = new TransRecord();
						transRecord.setRowKey(qd.getVariable());
						transRecord.setContent(qd.getValue());
						records.add(transRecord);
					}

				}
			} else {
				response.getWriter().print("error: no query");
				return;
			}

			for (String key : parms.keySet()) {
				System.out.println(key + " : " + parms.get(key));
				TransRecord transRecord = new TransRecord();
				transRecord.setRowKey(key);
				transRecord.setContent(parms.get(key));
				records.add(transRecord);
			}
			JSONArray jsonArray = new JSONArray(records);

			response.getWriter().print(jsonArray.toString());
		} else {
			response.getWriter().print("error: no msg from web page.");
		}
	}

	private Map<String, String> parseJson(String jsonString) {
		Map<String, String> result = new HashMap<String, String>();
		JSONObject jo = null;
		try {
			jo = new JSONObject(jsonString);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		@SuppressWarnings("unchecked")
		Iterator<String> keysItr = jo.keys();
		while (keysItr.hasNext()) {
			String key = keysItr.next();
			Object value = null;
			try {
				value = jo.get(key);
				if (value == null) {
					throw new JSONException("no match value");
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			result.put(key, value.toString());
		}
		return result;
	}

}
